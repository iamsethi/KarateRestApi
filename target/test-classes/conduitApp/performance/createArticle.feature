@parallel=false
Feature: Tests the login page

  Background: Define URL
    #refer karate-config.js
    Given url apiUrl
    * def articleRequestBody = read('classpath:conduitApp/json/request.json')
    * def dataGenerator = Java.type('helpers.DataGenerator')

 #   * set articleRequestBody.article.title = __gatling.Title
 #   * set articleRequestBody.article.description = __gatling.description

    * set articleRequestBody.article.title = dataGenerator.getRandomArticlesValues().title
    * set articleRequestBody.article.description = dataGenerator.getRandomArticlesValues().description
    * set articleRequestBody.article.body = dataGenerator.getRandomArticlesValues().body

    * def sleep = function(ms){ java.lang.Thread.sleep(ms)}
    * def pause = karate.get('_gatling.pause',sleep)

  @delete
  Scenario: Create and delete article
    Given path 'articles'
    And request articleRequestBody
    When method Post
    Then status 200
    * def articleId = response.article.slug

   # * pause(5000)

    Given path 'articles',articleId
    When method Delete
    Then status 200