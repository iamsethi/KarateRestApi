@parallel=false
Feature: Tests the login page

  Background: Define URL
    #refer karate-config.js
    Given url apiUrl
    * def articleRequestBody = read('classpath:conduitApp/json/request.json')
    * def dataGenerator = Java.type('helpers.DataGenerator')
    * set articleRequestBody.article.title = dataGenerator.getRandomArticlesValues().title
    * set articleRequestBody.article.description = dataGenerator.getRandomArticlesValues().description
    * set articleRequestBody.article.body = dataGenerator.getRandomArticlesValues().body

  @postpost
  Scenario: Create a new article
    Given path 'articles'
    And request articleRequestBody
    When method Post
    Then status 200
    And match response.article.title == articleRequestBody.article.title

  @delete
  Scenario: Create and delete article
    Given path 'articles'
    And request articleRequestBody
    When method Post
    Then status 200
    * def articleId = response.article.slug

    Given params {limit: 10,offset:0}
    Given path 'articles'
    When method Get
    Then status 200
    And match response.articles[0].title == articleRequestBody.article.title

    Given path 'articles',articleId
    When method Delete
    Then status 200

    Given params {limit: 10,offset:0}
    Given path 'articles'
    When method Get
    Then status 200
    And match response.articles[0].title != articleRequestBody.article.title