@regression
Feature: Tests the home page

  Background: Define URL
    Given url apiUrl

  @tags @gets
  Scenario: Get all tags
    Given path 'tags'
    When method Get
    Then status 200
    And match response.tags contains 'HITLER'
    And match response.tags contains ['HpapauITLER','Gandhi']
    And match response.tags !contains 'truck'
    And match response.tags contains any ['fish','dog','SIDA']
    And match response.tags == '#array'
    And match each response.tags == '#string'

  @search @get
  Scenario: Get 10 articles from the page
    * def timeValidator = read('classpath:helpers/timeValidator.js')
    Given params { limit: 10,offset: 0}
    Given path 'articles'
    When method Get
    Then status 200

    And match response.articles == '#[10]'
    And match response.articlesCount == 500
    And match response == {"articles": "#[10]","articlesCount":500}
    And match response.articlesCount != 501
    And match response.articles[0].createdAt contains '2020'
    And match response.articles[*].favoritesCount contains 2
    And match response..bio contains null

    # meaning of ##string is any one of 'bio' should be string
    # meaning of #string is every 'bio' value should be string
    And match each response.articles ==
    """
       {
          "title": "#string",
          "slug": "#string",
          "body": "#string",
          "createdAt": "#? timeValidator(_)",
          "updatedAt": "#? timeValidator(_)",
          "tagList": "#array",
          "description": "#string",
          "author": {
              "username": "#string",
              "bio": "##string",
              "image": "#string",
              "following": "#boolean"
          },
          "favorited": "#boolean",
          "favoritesCount": "#number"
       }
    """

  @logic
  Scenario: Conditional logic
    Given params {limit: 10, offset: 0}
    And path 'articles'
    When method Get
    Then status 200
    * def favoritesCount = response.articles[0].favoritesCount
    * def article = response.articles[0]

    * if (favoritesCount == 0) karate.call('classpath:helpers/AddLikes2.feature',article)

    Given params {limit: 10, offset: 0}
    And path 'articles'
    When method Get
    Then status 200
    And match response.articles[0].favoritesCount == 1

  @retry
  Scenario: Retry call
    * configure retry = { count: 5, interval: 5000}

    Given params {limit: 10, offset: 0}
    And path 'articles'
    And retry until response.articles[0].favouritesCount == 1
    When method Get
    Then status 200

  @sleep
  Scenario: Sleep call
    * def sleep = function(pause) { java.lang.Thread.sleep(pause)}

    Given params {limit: 10, offset: 0}
    And path 'articles'
    When method Get
    * eval sleep(5000)
    Then status 200

  @conversion
  Scenario: Number to string
    * def foo = 10
    * def json = {"bar": #(foo+'')}
    * match json == {"bar": '10'}

  @stringToNumber
  Scenario: String to number
    * def foo = '10'
    #first method
    * def json = {"bar":#(foo*1)}
    #second method
    * def json2 = {"bar":#(parseInt(foo))}
    * match json == {"bar": 10}