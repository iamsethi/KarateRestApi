@hooks
Feature: Hooks


  Background: hooks
   # * def result = callonce read('classpath:helpers/Dummy.feature')
  #  * def username = result.username

    #after hooks
    # below will run after scenario is completed
    #in line js function
    * configure afterScenario = function(){karate.call('classpath:helpers/Dummy.feature')}
      # below will run after feature is completed
    #multi line js function
    * configure afterFeature =
  """
    function(){
    karate.log('I will be printed after feature is completed');
    }
  """


  Scenario: First scenario
    * print 'This is first scenario'

  Scenario: Second scenario
    * print 'This is second scenario'