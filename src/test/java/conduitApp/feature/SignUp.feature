Feature: Sign Up new user


  Background: Define URL
    * def dataGenerator = Java.type('helpers.DataGenerator')
    * def timeValidator = read('classpath:helpers/timeValidator.js')

        #using static method from DataGenerator class
    * def randomEmail =  dataGenerator.getRandomEmail()
    * def randomUsername =  dataGenerator.getRandomUsername()

        #Also we can use non static method from DataGenerator class using javascript
    * def jsFunction =
    """
     function() {
        var DataGenerator = Java.type('helpers.DataGenerator')
        var generator = new DataGenerator()
        return generator.getRandomUsername2()
     }
    """

    * def randomUsername2 = call jsFunction

    Given url apiUrl

  @Post @signUp
  Scenario: New user Sign Up



    Given path 'users'
    And request
    """
        {
        "user": {
            "email": #(randomEmail),
            "password": "qwertyiop",
            "username": #(randomUsername2)
        }
        }
    """
    When method Post
    Then status 200
    And match response ==
    """
{
    "user": {
        "id": "#number",
        "email": "#(randomEmail)",
        "createdAt": "#? timeValidator(_)",
        "updatedAt": "#? timeValidator(_)",
        "username": "#(randomUsername2)",
        "bio": null,
        "image": null,
        "token": "#string"
    }
}
    """

  @invalidUser
  Scenario Outline:  Validate Sign up error messages

    Given path 'users'
    And request
    """
      {
        "user": {
            "email": "<email>",
            "password": "<password>",
            "username": "<username>"
        }
      }
    """
    When method Post
    Then status 422
    And match response == <errorResponse>
    Examples:
      | email          | password  | username          | errorResponse                                      |
      | #(randomEmail) | Karate123 | KarateUser123     | {"errors":{"username":["has already been taken"]}} |
      | 123@test.com   | Karate123 | #(randomUsername) | {"errors":{"email":["has already been taken"]}}    |
