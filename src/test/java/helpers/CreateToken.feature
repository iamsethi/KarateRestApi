Feature: Create Token

  #called in Karate-config.js
  Scenario: Create Token
    Given url apiUrl
    And path 'users/login'
     #refer karate-config.js for email and password
    And request {"user": {"email": "#(userEmail)","password": "#(userPassword)"}}
    When method Post
    Then status 200
    * def authToken = response.user.token