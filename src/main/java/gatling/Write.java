package gatling;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

public class Write {
    public static void main(String[] args) {
        // Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        // Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("student Details");
        Random rn = new Random();
        // This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[]{"ScenarioName", "no of request", "Ko", "Gatling", "Prism", "Prism Mean"
        });
        data.put("2", new Object[]{"a", 1 + rn.nextInt(10), 0, 923, 375, 375});
        data.put("3", new Object[]{"b", 1, 0, 923 + rn.nextInt(10), 375, 375});
        data.put("4", new Object[]{"c", 1, 0, 923, 375 + rn.nextInt(10), 375});
        data.put("5", new Object[]{"d", 1, 0, 923, 375, 375 + rn.nextInt(10)});

        // Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof String)
                    cell.setCellValue((String) obj);
                else if (obj instanceof Integer)
                    cell.setCellValue((Integer) obj);
            }
        }
        try {
            String userDir = System.getProperty("user.dir");
            FileOutputStream out = new FileOutputStream(new File(userDir + "/target/gatling/build.xlsx"));
            workbook.write(out);
            out.close();
            System.out.println("written successfully on disk.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
