mvn test "-Dkarate.options=--tags @debug"
url
path 'articles',articleId e.g /articles/some-title-n3h45q
params { key:value , key:value} 
request {}
method Get/Post/Delete
status 200
match response.tags contains ['HuManIty','Gandhi'] e.g  "tags": [ "HuManIty","Hu‌Man‌Ity","Gandhi", "HITLER" ]
match response.tags == "#array"
And match each response.tags == "#string" e.g  "tags": [ "HuManIty","Hu‌Man‌Ity","Gandhi", "HITLER" ] #each response is string
match response.tags !contains 'Gandhi2'
match response.articles =='#[10]' e.g "articles": [{}{}{}{}{}{}{}{}{}{}]
* def x = response.y.z


And match each response..folllowing == false
And match each response..folllowing == '#boolean'
And match each response..favoritesCount == '#number'
And match each response..bio == '##string'

{
    "articles": [
        {
            "title": "Test Post",
            "slug": "test-post-t6rl57",
            "body": "Type Post Text",
            "createdAt": "2020-10-28T12:46:38.897Z",
            "updatedAt": "2020-10-28T12:46:38.897Z",
            "tagList": [],
            "description": "Type Post desc",
            "author": {
                "username": "ambika",
                "bio": null,
                "image": "https://static.productionready.io/images/smiley-cyrus.jpg",
                "following": false
            },
            "favorited": false,
            "favoritesCount": 1
        }
    ],
    "articlesCount": 500
}

Run Gatling test

mvn clean test-compile gatling:test